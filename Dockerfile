FROM golang:1.19-alpine as builder
WORKDIR /build
COPY . .
RUN go build -o /hltest main.go


FROM alpine:3
WORKDIR /app
COPY --from=builder hltest /app
ENTRYPOINT ["/app/hltest"]
