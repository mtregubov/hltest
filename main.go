package main

import (
	"log"
	"net/http"
)

const resp = `{
	"toggles": [
	  {
		"name": "lms-integration",
		"enabled": true,
		"variant": {
		  "name": "disabled",
		  "enabled": false
		},
		"impressionData": false
	  },
	  {
		"name": "selection-feedback-to-planning",
		"enabled": true,
		"variant": {
		  "name": "disabled",
		  "enabled": false
		},
		"impressionData": false
	  },
	  {
		"name": "doc-generator",
		"enabled": true,
		"variant": {
		  "name": "disabled",
		  "enabled": false
		},
		"impressionData": false
	  },
	  {
		"name": "schedule",
		"enabled": true,
		"variant": {
		  "name": "disabled",
		  "enabled": false
		},
		"impressionData": false
	  }
	]
  }`

func handleTogle(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp))
}
func main() {
	log.Println("Starting server on 8080...")
	http.HandleFunc("/", handleTogle)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
